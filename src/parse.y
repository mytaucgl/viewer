%{

#if (defined _MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>

#include "Wrl_lexer.hpp"

#define YYERROR_VERBOSE 1
#define YYDEBUG         1

extern int yylex();
extern void yyerror(const char * s);

 std::string * coord_str_ptr = 0;
 std::string * coord_index_str_ptr = 0;

%}

%union {
  std::string * text;
}

%type <text> NUMBER STRING_LITERAL TRUE FALSE
%type <text> IDENTIFIER
%type <text> Id nodeTypeId nodeNameId fieldId
%type <text> sfValues sfValue
%type <text> sfint32Values
%type <text> sfboolValue
%type <text> sfstringValue

%token IDENTIFIER
%token STRING_LITERAL
%token NUMBER
%token TRUE FALSE
%token DEF USE
%token TO ROUTE

%token LS
%token RS
%token AT
%token AND
%token OR
%token LE
%token GE
%token EQ
%token NE

%start Start

%%

/* General: */

Start           : vrmlScene
                ;

vrmlScene       : statements
                ;

statements      : /* empty */
                | statements statement
                ;

statement       : nodeStatement
                ;

nodeStatement   : node
                | DEF nodeNameId node
                | USE nodeNameId
                ;

nodeStatements  : nodeStatement
                | nodeStatements nodeStatement
                ;

/* nodes: */

node            : nodeTypeId '{' nodeBody '}'
                ;

nodeBody        : /* empty */
                | nodeBody fieldId sfValue
                | nodeBody fieldId '[' sfValues ']'
                {
                  if ((*$2) == "point") {
                    coord_str_ptr = $4;
                  } else if ((*$2) == "coordIndex") {
                    coord_index_str_ptr = $4;
                  }
                }
                | nodeBody fieldId '[' sfstringValues ']'
                | nodeBody fieldId sfnodeValue
                | nodeBody fieldId '[' ']'
                | nodeBody fieldId '[' nodeStatements ']'
                ;

sfstringValues  : sfstringValue
                | sfstringValues sfstringValue
                | sfstringValues "," sfstringValue
                ;

nodeNameId      : Id
                ;

nodeTypeId      : Id
                ;

fieldId         : Id
                ;

Id              : IDENTIFIER
                ;

/* Fields: */

sfValue         : sfint32Values { $$ = $1; }
                | sfboolValue { $$ = $1; }
                | sfstringValue { $$ = $1; }
                ;

sfnodeValue     : nodeStatement
                ;

sfint32Values   : NUMBER { $$ = $1; }
                | sfint32Values NUMBER { (*$1) += " " + (*$2); $$ = $1; }
                ;

sfboolValue     : TRUE { $$ = $1; }
                | FALSE { $$ = $1; }
                ;

sfstringValue   : STRING_LITERAL { $$ = $1; }
                ;

sfValues        : sfint32Values { $$ = $1; }
                ;

%%

/*! global yyerror */
void yyerror(const char * message)
{
  Wrl_lexer::instance()->yyerror(message, (int) yychar);
}

/*! global yylex */
int yylex(void)
{
  return Wrl_lexer::instance()->yylex();
}
